package org.test;

import java.util.logging.Logger;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class Excavation {
public static final Logger logger= Logger.getLogger(Excavation.class.getName());
	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.println("Invalid arguments/ invalid number of arguments");
			System.exit(0);
		}
		SparkConf conf = new SparkConf();
		conf.setAppName("ExpeditionsTest");
		conf.setMaster("local");
		JavaSparkContext jsc = new JavaSparkContext(conf);
		System.out.println(args[0]);
		try {
			String header = jsc.textFile(args[0]).first();
			jsc.textFile(args[0]).filter(row -> row != header).mapToPair(new PairFunction<String, String, Integer>() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Tuple2<String, Integer> call(String line) throws Exception {
					String[] fields = line.split(",");
					logger.fine(fields[4]+" : "+ fields[3]);
					return new Tuple2<String, Integer>(fields[4], Integer.parseInt(fields[3]));
				}

			}).reduceByKey(new Function2<Integer, Integer, Integer>() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Integer call(Integer v1, Integer v2) throws Exception {

					return v1 + v2;
				}
			}).map(new Function<Tuple2<String,Integer>, String>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public String call(Tuple2<String, Integer> v1) throws Exception {
					// TODO Auto-generated method stub
					return v1.toString();
				}
			}).saveAsTextFile(args[1]);
		} catch (Exception e) {
		}
		jsc.close();
	}

}